# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 11:26:15 2020

@author: Arthur Christianen

Make the grid for the integration to find the density of states. The distances are in atomic units
Returns grid in form [r1 r2 0 th1 th2 ph weight]
"""

import numpy as np
from numpy.polynomial.legendre import leggauss
from numpy.polynomial.chebyshev import chebgauss


def makegrid(r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lphi,label):
    r1=np.arange(r1min,r1max+dr1,dr1)
    r2=np.arange(r2min,r2max+dr2,dr2)
    cth1,wth1=leggauss(lth1)
    th1=np.arccos(cth1)
    cth2,wth2=leggauss(lth2)
    th2=np.arccos(cth2)
    #Note that the integration over phi goes from 0 to 2 pi. 
    #However, due to the symmetry of the problem, only integration from 0 to pi is necessary, with doubled weights
    ph=np.pi*np.arange(0,lphi,1)/lphi+np.pi/2/lphi
    wph=2*np.pi*np.ones(lphi)/lphi 
    R1,R2,TH1,TH2,PH=np.meshgrid(r1,r2,th1,th2,ph)
    R1,R2,WTH1,WTH2,WPH=np.meshgrid(r1,r2,wth1,wth2,wph)
    R1r=np.ravel(R1)
    R2r=np.ravel(R2)
    TH1r=np.ravel(TH1)
    TH2r=np.ravel(TH2)
    PHr=np.ravel(PH)
    WTH1=np.ravel(WTH1)
    WTH2=np.ravel(WTH2)
    WPH=np.ravel(WPH)
    Wtot=WTH1*WTH2*WPH*dr1*dr2
    q=np.transpose(np.concatenate([[R1r],[R2r],np.zeros([1,len(R1r)]),[TH1r],[TH2r],[PHr],[Wtot]]))
    np.savetxt('grids/grid'+label+'.note',q)