# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 15:06:04 2020

@author: Arthur Christianen 

Total procedure to calculate DOS and laser excitation rate toe a single excited state for AB+AB system, taking into account both the AB+AB and the A2+B2 configuration
First we make the integration grid, which is done using the Python function in make_grid.py
Then we calculate the energy and transition dipole moment. Here we use Python scripts to do this, and very simple Lennard-Jones potentials, 
but in the subprocess.run argument any other potential can be evaluated
Finally we calculate the DOS and laser excitation rate using functions in the file CalcDOS.py
The outputted DOS is in microK^-1, and the outputted excitation rate in microsecond^{-1} 
"""

import os
import numpy as np
import subprocess
from make_grid import *
from CalcDOS import *
import time
import datetime

#make relevant directories
print('Date software:21/10/2020')
print('Date',datetime.datetime.now())


#In grids/ the grids for calculation, energies and tdms are stored
if not os.path.exists('grids'):
    os.mkdir('grids')
#In results/ the resulting DOS and excitation rates are printed
if not os.path.exists('results'):
    os.mkdir('results')


#define constants
amu=1822.8885
cm1=1.0/2.19474631e5
joule  = 1/4.35974381e-18  
Ang    = 1/0.5291772083  
c0     = 299792458  
second = 1/2.418884326500e-17 
c0au   = c0*1e+10*Ang/second 
Wcm2=joule/(10**8*Ang)**2/second
#set masses
mA=40*amu
mB=87*amu
#Set laser intensity
Itot=1e3*Wcm2

print('make grids')
#set grid parameters for AB+AB
#bond length of diatom 1 in atomic units
r1min=3
r1max=9
dr1=0.5

#bond length of diatom 2 in atomic units
r2min=3
r2max=9
dr2=0.5

#grid size for theta1, theta2 and phi
lth1=8
lth2=8
lph=8


t1=time.time()

#make grid in r1,r2,th1,th2,ph
makegrid(r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lph,'ABAB')
makegrid(r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lph,'ABAB_ex')

#set grid parameters of A_2+B_2
#bond length of diatom 1 in atomic units
r1min=3
r1max=9
dr1=0.5

#bond length of diatom 2 in atomic units
r2min=3
r2max=9
dr2=0.5

#grid size for theta1, theta2 and phi
lth1=8
lth2=8
lph=8

#grid for big R. Here we use the same grid for big R for both the ABAB and A2B2 configuration
Rmin=3.0
Rmax=15.0
dR=1.0
Rgrid=np.arange(Rmin,Rmax+dR,dR)
lR=len(Rgrid)


t1=time.time()
#make grid in r1,r2,th1,th2,ph
makegrid(r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lph,'A2B2')
makegrid(r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lph,'A2B2_ex')
t2=time.time()
print('grids finished, TIME', t2-t1)
#%%
t1=time.time()

print('Calculate energies')
#calculate potential in grid. Replace this by your own potential

#Lennard-Jones parameters of ground state curve
sigm_g=5 #classical turning point in a.u.
eps_g=1000 #well depth in cm^-1

#Lennard-Jones parameters of excited state curve
sigm_ex=5.1
eps_ex=3000
off_ex=12000 #offset with respect to ground state potential in cm^-1

#Calculate energies. Can also be done in parallel
for iR in range(0,lR):
    R=Rgrid[iR]
    print('R=',R)
    #Energies are calculated in cm^-1
    subprocess.call('python LJpot_A2B2.py %f %f %f %f %f %f %s' %(mA,mB,R,sigm_g,eps_g,0,'A2B2'),shell=True)
    subprocess.call('python LJpot_ABAB.py %f %f %f %f %f %f %s' %(mA,mB,R,sigm_g,eps_g,0,'ABAB'),shell=True)
    subprocess.call('python LJpot_A2B2.py %f %f %f %f %f %f %s' %(mA,mB,R,sigm_ex,eps_ex,off_ex,'A2B2_ex'),shell=True)
    subprocess.call('python LJpot_ABAB.py %f %f %f %f %f %f %s' %(mA,mB,R,sigm_ex,eps_ex,off_ex,'ABAB_ex'),shell=True)
    #This returns the square of the transition dipole moment defined in atomic units
    subprocess.call('python faketdm.py %f %s' %(R,'A2B2'),shell=True)
    subprocess.call('python faketdm.py %f %s' %(R,'ABAB'),shell=True)

    
t2=time.time()
print('Energies calculated, TIME',t2-t1)
#%% Calculate DOS

#List of energies for which the DOS should be calculated, the zero of energy is set by E0
#The energies at which the DOS is calculated is given by Elist+E0-V(coords)
Elist=-np.arange(5.0,4450.1,5.0)*cm1
#Set the zero point energy, in our case this is the energy in the initial asymptotic limit
E0=-2000*cm1


t1=time.time()
#Define the grid of laser frequencies for which to calculate the excitation rate
gaprange=np.arange(0,20000,100.0)*cm1

print('Calculate DOS')
DOSR1=np.zeros([lR,len(Elist)])
DOSR2=np.zeros([lR,len(Elist)])
einB1R=np.zeros([lR,len(gaprange)])
einB2R=np.zeros([lR,len(gaprange)])

#Calculate the DOS and excitation rate as a function of R, this could also be done in parallel
for iR in range(0,lR):
    R=Rgrid[iR]
    print('R=',R)
    #Just the DOS
    #DOSR1[iR,:]=Calcdos_A2B2(mA,mB,Elist,R,E0,'A2B2')*dR
    #DOSR2[iR,:]=Calcdos_ABAB(mA,mB,Elist,R,E0,'ABAB')*dR
    #DOS and excitation rate
    Do,Ex=Calcdos_A2B2_ex(mA,mB,Elist,R,E0,gaprange,'A2B2')
    DOSR1[iR,:]=Do*dR
    einB1R[iR,:]=Ex*dR
    Do,Ex=Calcdos_ABAB_ex(mA,mB,Elist,R,E0,gaprange,'ABAB')
    DOSR2[iR,:]=Do*dR
    einB2R[iR,:]=Ex*dR

#Sum over R
DOS1=np.sum(DOSR1,axis=0) 
DOS2=np.sum(DOSR2,axis=0) 

einB1=np.sum(einB1R,axis=0)
einB2=np.sum(einB2R,axis=0)

#Sum contributions from both arrangements
DOS=(DOS1+DOS2)*10**-6 #DOS in microKelvin^-1 
einB=(einB1+einB2)*10**-6 

#Calculate excitation rate
exrate=einB*Itot*4*np.pi/3/c0au/DOS[0]*10**-6*second #excitation rate in microsecond^-1
t2=time.time()
print('DOS calculated, TIME',t2-t1)

#Return the DOS as a function of
np.savetxt('results/DOS.note',np.transpose([Elist,DOS]))
np.savetxt('results/exrate.note',np.transpose([gaprange,exrate]))


