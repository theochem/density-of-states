#Author A. Christianen
#22-11-2018
#This file contains the coordinate transformations from Jacobi coordinates to atomic distance coordinates and back
#All distances are in atomic units
# In this file atoma 1 and 2 are of type A and atoms 3 and 4 are of type B

import numpy as np	

# Change coordinates from interatomic distances to A2-B2 Jacobi coordinates
# coAD: atomic distances
# coJ:  Jacobi coordinates
#Input format:
#[r12,r13,r23,r14,r24,r34]
#Output format:
#[r1,r2,R,th1,th2,phi]
def changecoords1(coAD):
	coJ=np.zeros(np.shape(coAD))
	R=np.sqrt(coAD[:,1]**2+coAD[:,2]**2+coAD[:,3]**2+coAD[:,4]**2-coAD[:,0]**2-coAD[:,5]**2)/2
	costh1=(coAD[:,3]**2+coAD[:,4]**2-coAD[:,1]**2-coAD[:,2]**2)/4/R/coAD[:,0]
#     to avoid singularities
	ind=np.where(costh1>=1)[0]
	costh1[ind]=0.999999999
	ind=np.where(costh1<=-1)[0]
	costh1[ind]=-0.999999999
	costh2=(coAD[:,2]**2+coAD[:,4]**2-coAD[:,1]**2-coAD[:,3]**2)/4/R/coAD[:,5]
	ind=np.where(costh2>=1)[0]
	costh2[ind]=0.999999999
	ind=np.where(costh2<=-1)[0]
	costh2[ind]=-0.999999999
	cosphi=(coAD[:,3]**2+coAD[:,2]**2-coAD[:,4]**2-coAD[:,1]**2+2*coAD[:,0]*coAD[:,5]*costh1*costh2)/(2*coAD[:,0]*coAD[:,5]*np.sqrt(1-costh1**2)*np.sqrt(1-costh2**2))
	ind=np.where(cosphi>1)[0]
	cosphi[ind]=0.999999999
	ind=np.where(cosphi<=-1)[0]
	cosphi[ind]=-0.999999999
	coJ[:,0]=coAD[:,0]
	coJ[:,1]=coAD[:,5]
	coJ[:,2]=R
	coJ[:,3]=np.arccos(costh1)
	coJ[:,4]=np.arccos(costh2)
	coJ[:,5]=np.pi-np.arccos(cosphi)
	return coJ

# Change coordinates from interatomic distances to AB-AB Jacobi coordinates
# coAD: atomic distances
# coJ:  Jacobi coordinates
#Input format:
#[r12,r13,r23,r14,r24,r34]
#Output format:
#[r1,r2,R,th1,th2,phi]
def changecoords2(coAD,mA,mB):
	coJ=np.zeros(np.shape(coAD))
	R=np.sqrt(mA*mB*(mA/mB*coAD[:,0]**2+mB/mA*coAD[:,5]**2+coAD[:,2]**2+coAD[:,3]**2-coAD[:,1]**2-coAD[:,4]**2))/(mA+mB)
	costh1=((coAD[:,2]**2-coAD[:,3]**2+mA/mB*coAD[:,0]**2-mB/mA*coAD[:,5]**2+(mA**2-mB**2)/(mB+mA)**2*(coAD[:,1]**2-coAD[:,4]**2)+(mB**2-mA**2)/mB/mA*R**2)/4/R/coAD[:,1])
# to avoid singularities
	ind=np.where(costh1>=1)[0]
	costh1[ind]=0.999999999
	ind=np.where(costh1<=-1)[0]
	costh1[ind]=-0.999999999
	costh2=(coAD[:,3]**2-coAD[:,2]**2+mA/mB*coAD[:,0]**2-mB/mA*coAD[:,5]**2+(mA**2-mB**2)/(mB+mA)**2*(coAD[:,4]**2-coAD[:,1]**2)+(mB**2-mA**2)/mB/mA*R**2)/4/R/coAD[:,4]
	ind=np.where(costh2>=1)[0]
	costh2[ind]=0.999999999
	ind=np.where(costh2<=-1)[0]
	costh2[ind]=-0.999999999
	cosphi=(coAD[:,2]**2+coAD[:,3]**2-mA/mB*coAD[:,0]**2-mB/mA*coAD[:,5]**2-(mA-mB)**2/(mA+mB)**2*(coAD[:,1]**2+coAD[:,4]**2)+R**2*(mA-mB)**2/mB/mA+8*mB*mA/(mB+mA)**2*coAD[:,1]*coAD[:,4]*costh1*costh2+4*R*(mA-mB)/(mB+mA)*(coAD[:,1]*costh1+coAD[:,4]*costh2))*(mB+mA)**2/8/mB/mA/coAD[:,1]/coAD[:,4]/np.sqrt(1-costh1**2)/np.sqrt(1-costh2**2)
	ind=np.where(cosphi>1)[0]
	cosphi[ind]=0.999999999
	ind=np.where(cosphi<=-1)[0]
	cosphi[ind]=-0.999999999
	coJ[:,0]=coAD[:,1]
	coJ[:,1]=coAD[:,4]
	coJ[:,2]=R
	coJ[:,3]=np.pi-np.arccos(costh1)
	coJ[:,4]=np.arccos(costh2)
	coJ[:,5]=np.arccos(cosphi)
	return coJ


# Change coordinates A2-B2 Jacobi coordinates to interatomic distances
# coAD: atomic distances
# coJ:  Jacobi coordinates
#Input format:
#[r1,r2,R,th1,th2,phi]
#Output format
#[r12,r13,r23,r14,r24,r34]
def invchangecoords1(coJ):
	coordsnew=np.zeros(np.shape(coJ))
	r12=coJ[:,0]
	r34=coJ[:,1]
	R=coJ[:,2]
	th1=coJ[:,3]
	th2=coJ[:,4]
	cth=np.cos(th2)
	sth=np.sin(th2)
	cth1=np.cos(th1)
	sth1=np.sin(th1)
	phi=np.pi-coJ[:,5]
	cphi=np.cos(phi)
	sphi=np.sin(phi)
	r13=np.sqrt((r34/2.*sth*cphi-r12/2.*sth1)**2+1./4.*r34**2*sth**2*sphi**2+(R-r12/2.*cth1-r34/2.*cth)**2)
	r24=np.sqrt((r34/2.*sth*cphi-r12/2.*sth1)**2+1./4.*r34**2*sth**2*sphi**2+(R+r12/2.*cth1+r34/2.*cth)**2)
	r23=np.sqrt((r34/2.*sth*cphi+r12/2.*sth1)**2+1./4.*r34**2*sth**2*sphi**2+(R-r12/2.*cth1+r34/2.*cth)**2)
	r14=np.sqrt((r34/2.*sth*cphi+r12/2.*sth1)**2+1./4.*r34**2*sth**2*sphi**2+(R+r12/2.*cth1-r34/2.*cth)**2)
	coordsnew[:,0]=r12	
	coordsnew[:,1]=r13
	coordsnew[:,2]=r23
	coordsnew[:,3]=r14
	coordsnew[:,4]=r24
	coordsnew[:,5]=r34
	return coordsnew


# Change coordinates AB-AB Jacobi coordinates to interatomic distances
# here th1=th21 and th2=th22
# coAD: atomic distances
# coJ:  Jacobi coordinates
#Input format:
#[r1,r2,R,th1,th2,phi]
 #Output format
#[r12,r13,r23,r14,r24,r34]
def invchangecoords2(coJ,mA,mB):
	coordsnew=np.zeros(np.shape(coJ))
	r13=coJ[:,0]
	r24=coJ[:,1]
	R=coJ[:,2]
	th1=np.pi-coJ[:,3]
	th2=coJ[:,4]
	cth=np.cos(th2)
	sth=np.sin(th2)
	cth1=np.cos(th1)
	sth1=np.sin(th1)
	phi=coJ[:,5]
	cphi=np.cos(phi)
	sphi=np.sin(phi)
	r34=mA/(mB+mA)*np.sqrt((r24*sth*cphi-r13*sth1)**2+r24**2*sth**2*sphi**2+((mB+mA)/mA*R-r13*cth1-r24*cth)**2)
	r12=mB/(mB+mA)*np.sqrt((r24*sth*cphi-r13*sth1)**2+r24**2*sth**2*sphi**2+((mB+mA)/mB*R+r13*cth1+r24*cth)**2)
	r14=np.sqrt((mB/(mB+mA)*r24*sth*cphi+mA/(mB+mA)*r13*sth1)**2+(mB/(mB+mA))**2*r24**2*sth**2*sphi**2+(R-mA/(mB+mA)*r13*cth1+mB/(mB+mA)*r24*cth)**2)
	r23=np.sqrt((mA/(mB+mA)*r24*sth*cphi+mB/(mB+mA)*r13*sth1)**2+(mA/(mB+mA))**2*r24**2*sth**2*sphi**2+(R+mB/(mB+mA)*r13*cth1-mA/(mB+mA)*r24*cth)**2)
	coordsnew[:,0]=r12	
	coordsnew[:,1]=r13
	coordsnew[:,2]=r23
	coordsnew[:,3]=r14
	coordsnew[:,4]=r24
	coordsnew[:,5]=r34
	return coordsnew


