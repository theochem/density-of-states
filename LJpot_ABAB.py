# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 12:09:07 2020

@author: Arthur Christianen 

Calculate simple Lennard-Jones potential for AB-AB arrangement and returns energy in cm^-1
"""
import numpy as np
import sys
from coordtrafos import *

#masses of the particles
mA=float(sys.argv[1])
mB=float(sys.argv[2])
#distance between molecular centers of mass
R=float(sys.argv[3])
#Classical turning point of Lennard-Jones potential (in a.u.)
sigm=float(sys.argv[4])
#Well depth in cm^-1
eps=float(sys.argv[5])
#Energy offset in cm^-1
offset=float(sys.argv[6])
#Label of coordinate grid to be used
label=str(sys.argv[7])

q=np.loadtxt('grids/grid%s.note' %label)




q[:,2]=R
coords=invchangecoords2(q[:,:6],mA,mB)
E=np.sum(4*eps*((sigm/coords)**12-(sigm/coords)**6),axis=1)+offset
np.savetxt('grids/E_R%d_%s.note' %(R,label),E)