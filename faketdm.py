# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 12:29:45 2020

@author: Arthur Christianen


Calculate square of transition dipole moment in a.u.
"""

import numpy as np
import sys

#Grid parameters
R=float(sys.argv[1])
label=str(sys.argv[2])

q=np.loadtxt('grids/grid%s.note' %label)

#In this simple example the TDM^2 is 1 everywhere
np.savetxt('grids/tdm_R%d_%s.note' %(R,label),np.ones(len(q[:,0])))