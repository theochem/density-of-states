# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 12:29:45 2020

@author: Arthur Christianen 

Calculate DOS in K^-1 for an AB+AB collision

Ref: Phys. Rev. A 100, 032708
"""

import numpy as np
from coordtrafos import *

#Only DOS, AB-AB configuration, outputs DOS in K^-1
def Calcdos_ABAB(mA,mB,Egrid,R,E0,label):
    cm1=1.0/2.19474631e5
    kelvin=315774.64
    #load coordinate grid
    q=np.loadtxt('grids/grid'+label+'.note')
    mu=mA*mB/(mA+mB)
    q[:,2]=R
    #Load energies
    V=np.loadtxt('grids/E_R%d_%s.note' %(R,label))*cm1-E0
    lEgrid=len(Egrid)
    DOSR=np.zeros(lEgrid)
    weights=q[:,6]
    #Calculate interatomic distances to judge arrrangements
    coAD=invchangecoords2(q[:,:6],mA,mB)
    for k in range(0,len(V)):
        r1=q[k,0]
        r2=q[k,1]
        if (V[k]<0):
            #Calculate the weight due to overlap between different arrangements
            #dec 1 decides whether you are in the A2-B2 arrangement of AB-AB
            dec1=(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,1]+coAD[k,4])+(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,2]+coAD[k,3])
            #dec 2 decides between the two AB-AB arrangements
            dec2=(coAD[k,2]+coAD[k,3])/(coAD[k,1]+coAD[k,2]+coAD[k,3]+coAD[k,4])
            if dec1>1.25:
                W1=1
            elif dec1<0.75:
                W1=0
            else: 
                W1=0.5+9.0/16.0*np.sin(np.pi*(dec1-1)/2.0/0.25)+1.0/16.0*np.sin(3*np.pi*(dec1-1)/2.0/0.25)
            if dec2>0.5625:
                W2=1
            elif dec2<0.4375:
                W2=0
            else: 
                W2=0.5+9.0/16.0*np.sin(np.pi*(dec2-0.5)/2.0/0.0625)+1.0/16.0*np.sin(3*np.pi*(dec2-0.5)/2.0/0.0625)
            if W1*W2>0:
                indmin=np.where(Egrid>V[k])
                R=q[k,2]
                th1=q[k,3]
                th2=q[k,4]
                phi=q[k,5]
                #calculate inertial tensor
                Imat=np.zeros([3,3])
                Imat[0,0]=mu*r1**2*np.sin(th1)**2+mu*r2**2*np.sin(th2)**2
                Imat[0,1]=-mu*(r1**2*np.cos(th1)*np.sin(th1)+r2**2*np.cos(th2)*np.sin(th2)*np.cos(phi))
                Imat[1,0]=Imat[0,1]
                Imat[0,2]=-mu*r2**2*np.cos(th2)*np.sin(th2)*np.sin(phi)
                Imat[2,0]=Imat[0,2]
                Imat[1,1]=mu*r1**2*np.cos(th1)**2+(mA+mB)*R**2/2.0+mu*r2**2*np.cos(th2)**2+mu*r2**2*np.sin(th2)**2*np.sin(phi)**2
                Imat[1,2]=-mu*r2**2*np.sin(th2)**2*np.sin(phi)*np.cos(phi)
                Imat[2,1]=Imat[1,2]
                Imat[2,2]=mu*r1**2+(mA+mB)*R**2/2.0+mu*r2**2*np.cos(th2)**2+mu*r2**2*np.sin(th2)**2*np.cos(phi)**2
                Iinv=np.linalg.inv(Imat)
                detI=np.linalg.det(Imat)
                #calculate D-matrix
                Dmat=np.zeros([3,3])
                Dmat[0,2]=mu*r2**2*np.sin(th2)**2
                Dmat[2,0]=mu*r1**2
                Dmat[1,1]=-mu*r2**2*np.sin(phi)
                Dmat[1,2]=-mu*r2**2*np.sin(th2)*np.cos(th2)*np.cos(phi)
                Dmat[2,1]=mu*r2**2*np.cos(phi)
                Dmat[2,2]=-mu*r2**2*np.sin(th2)*np.cos(th2)*np.sin(phi)
                KmK=np.diag([mu/2.0*r1**2,mu/2.0*(r2**2),mu/2.0*(r2**2)*np.sin(th2)**2])
                #The prefactor (mA+mB)/4*(mu/2)**2 is coming from the r1-r2-R sector, whereas the other factor is from the angular part
                detA=(mA+mB)/4*(mu/2)**2*np.linalg.det(KmK-np.transpose(Dmat).dot(Iinv).dot(Dmat)/2.0)
                #Geometry factor
                Geomfact=R**4*r1**4*r2**4*np.sin(th1)*np.sin(th2)/detI/np.sqrt(detA)
                DOSR[indmin]=DOSR[indmin]+weights[k]*Geomfact*np.absolute(Egrid[indmin]-V[k])**2*W1*W2  
    #We only integrate over one AB-AB arrangement instead of both, giving a factor two with respect to the paper
    DOSR=2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3*DOSR/(2*np.pi)**9/kelvin/2  
    return DOSR

#DOS and excitation rate, AB-AB configuration, outputs DOS in K^-1
def Calcdos_ABAB_ex(mA,mB,Egrid,R,E0,gaprange,label):
    cm1=1.0/2.19474631e5
    kelvin=315774.64
    #load coordinate grid
    q=np.loadtxt('grids/grid%s.note' %label)
    mu=mA*mB/(mA+mB)
    q[:,2]=R
    #load energies
    V=np.loadtxt('grids/E_R%d_%s.note' %(R,label))*cm1-E0
    V2=np.loadtxt('grids/E_R%d_%s_ex.note' %(R,label))*cm1-E0
    #gap between ground and excited state
    gap=V2-V
    dgap=np.diff(gaprange)[0]
    einB=np.zeros(len(gaprange))
    #load linestrength (square of transition dipole moment)
    tdm=np.loadtxt('grids/tdm_R%d_%s.note' %(R,label))
    lEgrid=len(Egrid)
    DOSR=np.zeros(lEgrid)
    weights=q[:,6]
    #Calculate interatomic distances to judge arrrangements
    coAD=invchangecoords2(q[:,:6],mA,mB)
    for k in range(0,len(V)):
        r1=q[k,0]
        r2=q[k,1]
        if (V[k]<0):
            #Calculate the weight due to overlap between different arrangements
            #dec 1 decides whether you are in the A2-B2 arrangement of AB-AB
            dec1=(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,1]+coAD[k,4])+(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,2]+coAD[k,3])
            #dec 2 decides between the two AB-AB arrangements
            dec2=(coAD[k,2]+coAD[k,3])/(coAD[k,1]+coAD[k,2]+coAD[k,3]+coAD[k,4])
            if dec1>1.25:
                W1=1
            elif dec1<0.75:
                W1=0
            else: 
                W1=0.5+9.0/16.0*np.sin(np.pi*(dec1-1)/2.0/0.25)+1.0/16.0*np.sin(3*np.pi*(dec1-1)/2.0/0.25)
            if dec2>0.5625:
                W2=1
            elif dec2<0.4375:
                W2=0
            else: 
                W2=0.5+9.0/16.0*np.sin(np.pi*(dec2-0.5)/2.0/0.0625)+1.0/16.0*np.sin(3*np.pi*(dec2-0.5)/2.0/0.0625)
            if W1*W2>0:
                indmin=np.where(Egrid>V[k])
                R=q[k,2]
                th1=q[k,3]
                th2=q[k,4]
                phi=q[k,5]
                #Calculate inertial tensor
                Imat=np.zeros([3,3])
                Imat[0,0]=mu*r1**2*np.sin(th1)**2+mu*r2**2*np.sin(th2)**2
                Imat[0,1]=-mu*(r1**2*np.cos(th1)*np.sin(th1)+r2**2*np.cos(th2)*np.sin(th2)*np.cos(phi))
                Imat[1,0]=Imat[0,1]
                Imat[0,2]=-mu*r2**2*np.cos(th2)*np.sin(th2)*np.sin(phi)
                Imat[2,0]=Imat[0,2]
                Imat[1,1]=mu*r1**2*np.cos(th1)**2+(mA+mB)*R**2/2.0+mu*r2**2*np.cos(th2)**2+mu*r2**2*np.sin(th2)**2*np.sin(phi)**2
                Imat[1,2]=-mu*r2**2*np.sin(th2)**2*np.sin(phi)*np.cos(phi)
                Imat[2,1]=Imat[1,2]
                Imat[2,2]=mu*r1**2+(mA+mB)*R**2/2.0+mu*r2**2*np.cos(th2)**2+mu*r2**2*np.sin(th2)**2*np.cos(phi)**2
                Iinv=np.linalg.inv(Imat)
                detI=np.linalg.det(Imat)
                #Calculate D-matrix
                Dmat=np.zeros([3,3])
                Dmat[0,2]=mu*r2**2*np.sin(th2)**2
                Dmat[2,0]=mu*r1**2
                Dmat[1,1]=-mu*r2**2*np.sin(phi)
                Dmat[1,2]=-mu*r2**2*np.sin(th2)*np.cos(th2)*np.cos(phi)
                Dmat[2,1]=mu*r2**2*np.cos(phi)
                Dmat[2,2]=-mu*r2**2*np.sin(th2)*np.cos(th2)*np.sin(phi)
                KmK=np.diag([mu/2.0*r1**2,mu/2.0*(r2**2),mu/2.0*(r2**2)*np.sin(th2)**2])
                #The prefactor (mA+mB)/4*(mu/2)**2 is coming from the r1-r2-R sector, whereas the other factor is from the angular part
                detA=(mA+mB)/4*(mu/2)**2*np.linalg.det(KmK-np.transpose(Dmat).dot(Iinv).dot(Dmat)/2.0)
                #Geometry factor
                Geomfact=R**4*r1**4*r2**4*np.sin(th1)*np.sin(th2)/detI/np.sqrt(detA)
                DOSR[indmin]=DOSR[indmin]+weights[k]*Geomfact*np.absolute(Egrid[indmin]-V[k])**2*W1*W2 
                gapindices=np.where(gaprange<gap[k])[0]
                if len (gapindices)>0:
                    gapind=np.amax(gapindices)
                    einB[gapind]=einB[gapind]+weights[k]*Geomfact*np.absolute(Egrid[0]-V[k])**2*W1*W2*tdm[k]
    #We only integrate over one AB-AB arrangement instead of both, giving a factor two with respect to the paper
    DOSR=2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3*DOSR/(2*np.pi)**9/kelvin/2 
    einB=einB*2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3/(2*np.pi)**9/kelvin/2/dgap 
    return DOSR,einB


#Only DOS, A2-B2 configuration. Outputs DOS in K^-1
def Calcdos_A2B2(mA,mB,Egrid,R,E0,label):
    cm1=1.0/2.19474631e5
    kelvin=315774.64
    #load coordinate grid
    q=np.loadtxt('grids/grid'+label+'.note')
    q[:,2]=R
    #load energies
    V=np.loadtxt('grids/E_R%d_%s.note' %(R,label))*cm1-E0
    lEgrid=len(Egrid)
    DOSR=np.zeros(lEgrid)
    weights=q[:,6]
    #Calculate interatomic distances to judge arrrangements
    coAD=invchangecoords1(q[:,:6])
    for k in range(0,len(V)):
        th1=q[k,3]
        th2=q[k,4]
        if (V[k]<0):
            #Calculate the weight due to overlap between different arrangements
            dec1=(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,1]+coAD[k,4])+(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,2]+coAD[k,3])
            if dec1>1.25:
                W1=0
            elif dec1<0.75:
                W1=1
            else: 
                W1=0.5-9.0/16.0*np.sin(np.pi*(dec1-1)/2.0/0.25)-1.0/16.0*np.sin(3*np.pi*(dec1-1)/2.0/0.25)
            if W1>0:
                indmin=np.where(Egrid>V[k])
                r1=q[k,0]
                r2=q[k,1]
                R=q[k,2]
                th1=q[k,3]
                th2=q[k,4]
                phi=q[k,5]
                #Calculate intertial tensor
                Imat=np.zeros([3,3])
                Imat[0,0]=mA/2.*r1**2*np.sin(th1)**2+mB/2.*r2**2*np.sin(th2)**2
                Imat[0,1]=-mA/2.*r1**2*np.cos(th1)*np.sin(th1)-mB/2.*r2**2*np.cos(th2)*np.sin(th2)*np.cos(phi)
                Imat[1,0]=Imat[0,1]
                Imat[0,2]=-mB/2.*r2**2*np.cos(th2)*np.sin(th2)*np.sin(phi)
                Imat[2,0]=Imat[0,2]
                Imat[1,1]=mA/2.*r1**2*np.cos(th1)**2+2*mB*mA/(mA+mB)*R**2+mB/2.*r2**2*np.cos(th2)**2+mB/2.*r2**2*np.sin(th2)**2*np.sin(phi)**2
                Imat[1,2]=-mB/2.*r2**2*np.sin(th2)**2*np.sin(phi)*np.cos(phi)
                Imat[2,1]=Imat[1,2]
                Imat[2,2]=mA/2.*r1**2+2*mB*mA/(mA+mB)*R**2+mB/2.*r2**2*np.cos(th2)**2+mB/2.*r2**2*np.sin(th2)**2*np.cos(phi)**2
                Iinv=np.linalg.inv(Imat)
                detI=np.linalg.det(Imat)
                #Calculate D-matrix
                Dmat=np.zeros([3,3])
                Dmat[0,2]=mB/2.*r2**2*np.sin(th2)**2
                Dmat[2,0]=mA/2.*r1**2
                Dmat[1,1]=-mB/2.*r2**2*np.sin(phi)
                Dmat[1,2]=-mB/2.*r2**2*np.sin(th2)*np.cos(th2)*np.cos(phi)
                Dmat[2,1]=mB/2.*r2**2*np.cos(phi)
                Dmat[2,2]=-mB/2.*r2**2*np.sin(th2)*np.cos(th2)*np.sin(phi)
                KmK=np.diag([mA/4.0*r1**2,mB/4.0*(r2**2),mB/4.0*(r2**2)*np.sin(th2)**2])
                #The prefactor mA**2*mB**2/16/(mA+mB) is coming from the r1-r2-R sector, whereas the other factor is from the angular part
                detA=mA**2*mB**2/16/(mA+mB)*np.linalg.det(KmK-np.transpose(Dmat).dot(Iinv).dot(Dmat)/2.0)
                #Geometry factor
                Geomfact=R**4*r1**4*r2**4*np.sin(th1)*np.sin(th2)/detI/np.sqrt(detA)
                DOSR[indmin]=DOSR[indmin]+weights[k]*Geomfact*np.absolute(Egrid[indmin]-V[k])**2*W1
    DOSR=2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3*DOSR/(2*np.pi)**9/kelvin/4  
    return DOSR

#DOS and excitation rate, A2-B2 configuration. Outputs DOS in K^-1.
def Calcdos_A2B2_ex(mA,mB,Egrid,R,E0,gaprange,label):
    cm1=1.0/2.19474631e5
    kelvin=315774.64
    #load coordinate grid
    q=np.loadtxt('grids/grid%s.note' %label)
    q[:,2]=R
    #load energies
    V=np.loadtxt('grids/E_R%d_%s.note' %(R,label))*cm1-E0
    V2=np.loadtxt('grids/E_R%d_%s_ex.note' %(R,label))*cm1-E0
    #gap between ground and excited states
    gap=V2-V
    dgap=np.diff(gaprange)[0]
    einB=np.zeros(len(gaprange))
    tdm=np.loadtxt('grids/tdm_R%d_%s.note' %(R,label))
    lEgrid=len(Egrid)
    DOSR=np.zeros(lEgrid)
    weights=q[:,6]
    #Calculate interatomic distances to judge arrangement
    coAD=invchangecoords1(q[:,:6])
    for k in range(0,len(V)):
        th1=q[k,3]
        th2=q[k,4]
        if (V[k]<0):
            #Calculate the weight due to overlap between different arrangements
            dec1=(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,1]+coAD[k,4])+(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,2]+coAD[k,3])
            if dec1>1.25:
                W1=0
            elif dec1<0.75:
                W1=1
            else: 
                W1=0.5-9.0/16.0*np.sin(np.pi*(dec1-1)/2.0/0.25)-1.0/16.0*np.sin(3*np.pi*(dec1-1)/2.0/0.25)
            if W1>0:
                indmin=np.where(Egrid>V[k])
                r1=q[k,0]
                r2=q[k,1]
                R=q[k,2]
                th1=q[k,3]
                th2=q[k,4]
                phi=q[k,5]
                #Calculate inertial tensor
                Imat=np.zeros([3,3])
                Imat[0,0]=mA/2.*r1**2*np.sin(th1)**2+mB/2.*r2**2*np.sin(th2)**2
                Imat[0,1]=-mA/2.*r1**2*np.cos(th1)*np.sin(th1)-mB/2.*r2**2*np.cos(th2)*np.sin(th2)*np.cos(phi)
                Imat[1,0]=Imat[0,1]
                Imat[0,2]=-mB/2.*r2**2*np.cos(th2)*np.sin(th2)*np.sin(phi)
                Imat[2,0]=Imat[0,2]
                Imat[1,1]=mA/2.*r1**2*np.cos(th1)**2+2*mB*mA/(mA+mB)*R**2+mB/2.*r2**2*np.cos(th2)**2+mB/2.*r2**2*np.sin(th2)**2*np.sin(phi)**2
                Imat[1,2]=-mB/2.*r2**2*np.sin(th2)**2*np.sin(phi)*np.cos(phi)
                Imat[2,1]=Imat[1,2]
                Imat[2,2]=mA/2.*r1**2+2*mB*mA/(mA+mB)*R**2+mB/2.*r2**2*np.cos(th2)**2+mB/2.*r2**2*np.sin(th2)**2*np.cos(phi)**2
                Iinv=np.linalg.inv(Imat)
                detI=np.linalg.det(Imat)
                #Calculate D-matrix
                Dmat=np.zeros([3,3])
                Dmat[0,2]=mB/2.*r2**2*np.sin(th2)**2
                Dmat[2,0]=mA/2.*r1**2
                Dmat[1,1]=-mB/2.*r2**2*np.sin(phi)
                Dmat[1,2]=-mB/2.*r2**2*np.sin(th2)*np.cos(th2)*np.cos(phi)
                Dmat[2,1]=mB/2.*r2**2*np.cos(phi)
                Dmat[2,2]=-mB/2.*r2**2*np.sin(th2)*np.cos(th2)*np.sin(phi)
                KmK=np.diag([mA/4.0*r1**2,mB/4.0*(r2**2),mB/4.0*(r2**2)*np.sin(th2)**2])
                #mA**2*mB**2/16/(mA+mB) is coming from the r1-r2-R sector, whereas the other factor is from the angular part
                detA=mA**2*mB**2/16/(mA+mB)*np.linalg.det(KmK-np.transpose(Dmat).dot(Iinv).dot(Dmat)/2.0)
                #Geometry factor
                Geomfact=R**4*r1**4*r2**4*np.sin(th1)*np.sin(th2)/detI/np.sqrt(detA)
                DOSR[indmin]=DOSR[indmin]+weights[k]*Geomfact*np.absolute(Egrid[indmin]-V[k])**2*W1
                gapindices=np.where(gaprange<gap[k])[0]
                if len (gapindices)>0:
                    gapind=np.amax(gapindices)
                    einB[gapind]=einB[gapind]+weights[k]*Geomfact*np.absolute(Egrid[0]-V[k])**2*W1*tdm[k]
    DOSR=2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3*DOSR/(2*np.pi)**9/kelvin/4 
    einB=einB*2*np.pi**6*(mB**6*mA**6)/(2*mB+2*mA)**3/(2*np.pi)**9/kelvin/4/dgap
    return DOSR,einB