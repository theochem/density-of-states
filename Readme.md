# Version v0.9.0
Date: 21-Oct-2020

# Purpose
This folder contains the necessary files to calculate the density of states and
laser excitation rates for ultracold AB+AB or A2+B2 collisions.  These codes
are based on the work done in:

1. A\. Christianen, T. Karman, G. C. Groenenboom, *Physical Review A* **100** (3), 032708
(2019)
2. A\. Christianen, M. W. Zwierlein, G. C. Groenenboom, T. Karman, 
   *Physical Review Letters* **123** (12), 123402 (2019)

The calculations yield the quantities of Eq. (32) of the PRA for AB+AB systems
(the DOS) and Eq. (3,5) of the PRL (laser excitation rate).

# Python version and packages
The codes are tested for Python 2.7 and only use the standard packages `numpy,
os, sys, subprocess, datetime and time`.

# Usage

- The file `simpleDOS_calc.py` contains a script which calls the necessary functions, and where all relevant parameters can be specified.
- Executing this script will lead to all necessary calculations being done.
  This script will create folders `./grids` and `./outputs`, where all results
will be stored.
- When python is installed this program can simply be run by: `python simpleDOS_calc.py`
- Its runtime should be a few minutes. 
- The main results are returned in the files `./output/DOS.note`, 
which returns the density of states as a function of the offset energy with
respect to the dissociation energy in the format `[E, DOS]`, where the energy
is in hartree and the DOS in microKelvin^-1 and the file
`./output/exrate.note`, which returns the excitation rate as a function of the
laser photon energy in the format `[E, exrate]`. The photon energy is in
hartree and the excitation rate in microsecond<sup>-1</sup>.
- The file `make_grid.py` contains a function to calculate the relevant
integration grids. Its input are the grid parameters specified in
`simpleDOS_calc.py`.
- Its output are the files `./grids/grid%d` (where `%d` depends on the
  arrangement) with the format `[r_1 r_2 0 th_1 th_2 phi, w]`.
- There is a `0` in this column which is left open for the value of `R`, which is
  looped over in the main script. All distances are in atomic units.
- The `w` stands for the integration weights corresponding to the arrangement.

# Potentials and transition dipole moment surfaces
The files `LJpot_ABAB.py`, `LJpot_A2B2.py` and `faketdm.py` are simple
functions that generate potentials and transition dipole moment surfaces.

- These files have to be replaced by the potential relevant for the system of study. 
- The input is in the form of a coordinate matrix with the following column
  order: `[r_1 r_2 R th_1 th_2 phi]`.
- For the input, all distances are in atomic units.
- The outputs of the potential functions are: `./grids/E_R%f_%d` where `%f` is
  the value of `R` and `%d` a string corresponding to the arrangement. 
- The output is a single column of energies in cm<sup>-1</sup> where the order
  is the same as the order of the corresponding grid.
- For the TDM the output is in `./grids/tdm_R%f_%d` where `%f` is the value of
  `R` and `%d` a string corresponding to the arrangement.  
- The output is a single column of square TDMs in atomic units where the order
  is the same as the order of the corresponding grid.

The file `CalcDOS.py` contains the functions that actually calculate the DOS
and excitation rates.  The file `coordtrafos.py` contains functions that do
some relevant coordinate transformations (from atomic distances to Jacobi
coordinates and back). 

For more information and the units used in several parts of the code, please
check the comments in the respective python files.

# How to converge results
To converge the results please test the following grid parameters in the file
`simpleDOS_calc.py`:

> `r1min,r1max,dr1,r2min,r2max,dr2,lth1,lth2,lph, Rmin, Rmax, dR`.

This needs to be done separately for both the AB-AB and A2-B2 arrangement. Note
that currently the same grid in `R` is used for both arrangements.

# Test results
If `simpleDOS_calc.py` is executed without modification, the output files
in `./results` should match the files in `./results_test`.
