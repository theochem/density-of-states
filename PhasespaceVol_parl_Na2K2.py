import numpy as np
import sys
from coordtrafos import *

amu=1822.8885
mA=40*amu
mB=87*amu	
Hart=2.19474631e5
cm1=1.0/2.19474631e5

laser=0.0428227/1.5
mu=mA*mB/(mA+mB)

mK=mA
mNa=mB


#dr1=0.5
#dr2=0.5
#r1list=np.arange(3,9,dr1)
#r2list=np.arange(3,9,dr2)
#lr1=len(r1list)
#lr2=len(r2list)

Rval=float(sys.argv[1])
#scaleE=float(sys.argv[2])
#scaleR=float(sys.argv[3])
E0=2000/Hart
label='A2B2'
#name='PSV_grid_NaKNaK/angcoords']
coords=np.loadtxt('grids/grid'+label+'.note')
coords[:,2]=Rval
coAD=invchangecoords1(coords[:,:6])
weights=coords[:,6]
Emin=-np.array([5.0])/Hart#np.arange(5.0,4450.1,5.0)/Hart*scaleE
#lEmin=len(Emin)
#PSV1=np.zeros([lEmin])

gaprange=np.arange(-10000,20000,100.0)*cm1
print(gaprange)
gapcats1=np.zeros(len(gaprange))
gapcats1tdm=np.zeros(len(gaprange))




#coAD=invchangecoords2(coords[:,:6],mA,mB)
E=np.loadtxt('grids/E_R%d_%s.note' %(Rval,label))*cm1+E0#np.fromfile('PSV_grid_NaKNaK/energyGP2500_3new-R_%2.1f-r1_%2.1f-r2_%2.1f' %(Rval,r1list[ir1],r2list[ir2]),dtype=float)/Hart+4.7843802E-02
V2=np.loadtxt('grids/E_R%d_%s_ex.note' %(Rval,label))*cm1+E0
gap=V2-E

#tdmtot=np.fromfile('PSV_grid_NaKNaK/tdmtotGP2500_1-R_%2.1f-r1_%2.1f-r2_%2.1f' %(Rval,r1list[ir1],r2list[ir2]),dtype=float)
tdm=np.loadtxt('grids/tdm_R%d_%s.note' %(Rval,label))

gaprange=np.arange(-10000,20000,100)/Hart
#print(gaprange)
gapcats1=np.zeros(len(gaprange))


gapcats1tdm=np.zeros(len(gaprange))


#print(coAD)


for k in range(0,len(E)):
      if E[k]<np.amin(Emin):
            #print(gap[k])
            dec1=(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,1]+coAD[k,4])+(coAD[k,0]+coAD[k,5])/2.0/(coAD[k,2]+coAD[k,3])
            #dec2=(coAD[k,2]+coAD[k,3])/(coAD[k,1]+coAD[k,2]+coAD[k,3]+coAD[k,4])
            if dec1>1.25:
                  W1=0
            elif dec1<0.75:
                  W1=1
            else: 
                  W1=0.5-9.0/16.0*np.sin(np.pi*(dec1-1)/2.0/0.25)-1.0/16.0*np.sin(3*np.pi*(dec1-1)/2.0/0.25)
            if W1>0:
                  #indmin=np.where(-E[k]>Emin)
                  indE=k
                  rK=coords[indE,0]
                  rNa=coords[indE,1]
                  R=coords[indE,2]
                  chi=coords[indE,3]
                  theta=coords[indE,4]
                  phi=coords[indE,5]
                  Imat=np.zeros([3,3])
                  Imat[0,0]=mK/2.*rK**2*np.sin(chi)**2+mNa/2.*rNa**2*np.sin(theta)**2
                  Imat[0,1]=-mK/2.*rK**2*np.cos(chi)*np.sin(chi)-mNa/2.*rNa**2*np.cos(theta)*np.sin(theta)*np.cos(phi)
                  Imat[1,0]=Imat[0,1]
                  Imat[0,2]=-mNa/2.*rNa**2*np.cos(theta)*np.sin(theta)*np.sin(phi)
                  Imat[2,0]=Imat[0,2]
                  Imat[1,1]=mK/2.*rK**2*np.cos(chi)**2+2*mNa*mK/(mK+mNa)*R**2+mNa/2.*rNa**2*np.cos(theta)**2+mNa/2.*rNa**2*np.sin(theta)**2*np.sin(phi)**2
                  Imat[1,2]=-mNa/2.*rNa**2*np.sin(theta)**2*np.sin(phi)*np.cos(phi)
                  Imat[2,1]=Imat[1,2]
                  Imat[2,2]=mK/2.*rK**2+2*mNa*mK/(mK+mNa)*R**2+mNa/2.*rNa**2*np.cos(theta)**2+mNa/2.*rNa**2*np.sin(theta)**2*np.cos(phi)**2
                  #print(indmin)
                  #
                  Iinv=np.linalg.inv(Imat)
                  detI=np.linalg.det(Imat)
                  #
                  Kmat=np.zeros([3,3])
                  Kmat[0,2]=mNa/2.*rNa**2*np.sin(theta)**2
                  Kmat[2,0]=mK/2.*rK**2
                  Kmat[1,1]=-mNa/2.*rNa**2*np.sin(phi)
                  Kmat[1,2]=-mNa/2.*rNa**2*np.sin(theta)*np.cos(theta)*np.cos(phi)
                  Kmat[2,1]=mNa/2.*rNa**2*np.cos(phi)
                  Kmat[2,2]=-mNa/2.*rNa**2*np.sin(theta)*np.cos(theta)*np.sin(phi)
                  Dmat=np.diag([mK/4.0*rK**2,mNa/4.0*(rNa**2),mNa/4.0*(rNa**2)*np.sin(theta)**2])
                  detA=np.linalg.det(Dmat-np.transpose(Kmat).dot(Iinv).dot(Kmat)/2.0)
                  order0=np.absolute(E[indE]-Emin)**2/np.sqrt(detA)
#
                  prefact=2*np.pi**6*R**4*rK**4*rNa**4*np.sin(chi)*np.sin(theta)/detI*(mNa**5*mK**5)/(mNa+mK)**(5./2.)*weights[k]#*dr1*dr2
                  gapind1=np.amax(np.where(gaprange<gap[k])[0])
                  gapcats1[gapind1]=gapcats1[gapind1]+prefact*order0*W1
                  gapcats1tdm[gapind1]=gapcats1tdm[gapind1]+prefact*order0*W1*tdm[k]

                              
gapcats=np.array([gapcats1,gapcats1tdm])
gapcats=gapcats/(2*np.pi)**9/315777.1  
#PSV1=PSV1/(2*np.pi)**9/315777.1  
print(np.sum(gapcats[0,:]))
#print(PSV1)

np.savetxt('results/PSV2_R%1.1f' %(Rval) ,gapcats)  
